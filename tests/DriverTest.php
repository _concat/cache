<?php

namespace Concat\Cache\Tests;

trait DriverTest
{
    protected $driver;

    protected function setUp()
    {
        $this->driver = $this->getDriver();

        // can we assume this works?
        $this->driver->clear();
    }

    protected function tearDown()
    {
        $this->driver->clear();
        $this->driver = null;
    }

    public function testString()
    {
        $this->setAndGetCompare("testString", "string");
    }

    public function testInt()
    {
        $this->setAndGetCompare("testInt", time());
    }

    public function testArray()
    {
        $this->setAndGetCompare("testArray", ["string", time()]);
    }

    public function testExistsAndDelete()
    {
        $this->driver->set("exists", 1);

        $this->assertTrue($this->driver->exists("exists"));
        $this->driver->delete("exists");

        $this->assertFalse($this->driver->exists("exists"));
    }

    public function testReplace()
    {
        $this->driver->set("testReplace", 1);
        $this->setAndGetCompare("testReplace", 2);
    }

    public function testClear()
    {
        $this->driver->set("testClear", 1);
        $this->driver->clear();
        $this->assertNull($this->driver->get("testClear"));
    }

    private function setAndGetCompare($key, $value)
    {
        $this->driver->set($key, $value);
        $this->assertEquals($value, $this->driver->get($key));
    }
}
