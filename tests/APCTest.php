<?php

namespace Concat\Cache\Tests;

use Concat\Cache\APC;

class APCTest extends \PHPUnit_Framework_TestCase
{
    use DriverTest;

    protected function getDriver()
    {
        return new APC(microtime());
    }
}
