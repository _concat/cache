<?php

namespace Concat\Cache;

use Concat\Filesystem\Directory;
use Concat\Filesystem\File;

class FileCache implements CacheInterface
{
    private $directory;

    public function __construct($directory = null)
    {
        if ($directory === null) {
            $directory = rtrim(sys_get_temp_dir(), "/")."/fc";
        }

        $this->setDirectory($directory);
    }

    public function setDirectory($path)
    {
        $this->directory = Directory::create($path);
    }

    public function get($name)
    {
        return File::getContents($this->path($name));
    }

    public function set($name, $value)
    {
        return File::putContents($this->path($name), $value);
    }

    private function path($name)
    {
        return join("/", [$this->directory, $name]);
    }

    public function delete($name)
    {
        return File::delete($this->path($name));
    }

    public function exists($name)
    {
        return File::exists($this->path($name));
    }

    public function clear()
    {
        if ($this->directory !== null && $this->directory->exists()) {
            return $this->directory->clear();
        }
    }
}
