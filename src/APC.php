<?php

namespace Concat\Cache;

class APC implements CacheInterface
{
    private $prefix;

    public function __construct($prefix = '')
    {
        $this->prefix = $prefix;

        if (!function_exists('apc_fetch')) {
            throw new \Exception("APC is required to use this driver");
        }
    }

    public function get($key)
    {
        if (apc_exists($this->prefix.$key)) {
            return apc_fetch($this->prefix.$key);
        }
    }

    public function set($key, $value)
    {
        return apc_store($this->prefix.$key, $value);
    }

    public function exists($key)
    {
        return apc_exists($this->prefix.$key);
    }

    public function delete($key)
    {
        return apc_delete($this->prefix.$key);
    }

    public function clear()
    {
        return apc_clear_cache();
    }
}
